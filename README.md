# Referral Services

## Development

### Requirements

- [golang](https://golang.org)
- [go-kit](https://github.com/go-kit/kit)
- [golang-migrate](https://github.com/golang-migrate/migrate)
- [postgresql](https://www.postgresql.org/)

### API Specification

#### V1

##### Auth

###### Login

Request :
- Method : `POST`
- Endpoint : `/v1/auth/login`
- Body :

    ```
    {
        "account": "string",
        "password": "string"
    }
    ```

Response :
- Success
  - Status Code : `200`
  - Body :

    ```
    {
        "success": boolean,
        "data": {
            "access_token": "string",
            "refresh_token": "string",
            "token_type": "string",
            "expired": integer
        }
    }
    ```

###### Register

Request :
- Method : `POST`
- Endpoint : `/v1/auth/register`
- Body :

    ```
    {
        "name": "string",
        "email": "string",
        "password": "string"
    }
    ```

Response :
- Success
  - Status Code : `201`
  - Body :

    ```
    {
        "success": boolean,
        "data": {
            "id": "string"
        }
    }
    ```

##### Products

###### Create Product

Request :
- Method : `POST`
- Endpoint : `/v1/products`
- Headers :
    - `Authorization` : "`string`"
- Body :

    ```
    {
        "name": "string",
        "type": "string",
        "weight": integer,
        "price": integer
    }
    ```

Response :
- Success
  - Status Code : `201`
  - Body :

    ```
    {
        "success": boolean,
        "data": {
            "id": "uuid"
        }
    }
    ```

###### Update Product

Request :
- Method : `PUT`
- Endpoint : `/v1/products/{id}`
- Headers :
    - `Authorization` : "`string`"
- Body :

    ```
    {
        "name": "string",
        "type": "string",
        "weight": integer,
        "price": integer
    }
    ```

Response :
- Success
  - Status Code : `200`
  - Body :

    ```
    {
        "success": boolean,
        "data": {
            "id": "uuid"
        }
    }
    ```

###### Delete Product

Request :
- Method : `DELETE`
- Endpoint : `/v1/products/{id}`
- Headers :
    - `Authorization` : "`string`"

Response :
- Success
  - Status Code : `200`
  - Body :

    ```
    {
        "success": boolean,
        "data": {
            "id": "uuid"
        }
    }
    ```

###### View Product

Request :
- Method : `GET`
- Endpoint : `/v1/products/{id}`
- Headers :
    - `Authorization` : "`string`"

Response :
- Success
  - Status Code : `200`
  - Body :

    ```
    {
        "success": boolean,
        "data": {
            "id": "uuid",
            "name": "string",
            "type": "string",
            "weight": integer,
            "price": integer
        }
    }
    ```

###### List Product

Request :
- Method : `GET`
- Endpoint : `/v1/products`
- Headers :
    - `Authorization` : "`string`"

Response :
- Success
  - Status Code : `200`
  - Body :

    ```
    {
        "success": boolean,
        "data": [
            {
                "id": "uuid",
                "name": "string",
                "type": "string",
                "weight": integer,
                "price": integer
            }
        ]
    }
    ```

##### Files

###### Upload

Request :
- Method : `POST`
- Endpoint : `/v1/files`
- Headers :
    - `Authorization` : "`string`"
- Body :

    ```
    {
        "title": "string",
        "data": "base64",
        "type": "string",
        "type_id": "uuid"
    }
    ```

Response :
- Success
  - Status Code : `201`
  - Body :

    ```
    {
        "success": boolean,
        "data": {
            "id": "uuid"
        }
    }
    ```

### Run
```
$ go run .
```

### Test

#### Unittest
@TODO
```
$ sh runtest.sh
```
#### Postman Collection

[postman_collection.json](https://gitlab.com/tukangremot/product-services/-/blob/master/docs/api/postman_collection.json)

## Deployment

### Run Docker
```
$ docker build -t product-services .
$ docker run --name product-services -p 8080:8080 -d product-services
```

