package main

import "os"

func config(key, fallback string) string {
	e := os.Getenv(key)
	if e == "" {
		return fallback
	}
	return e
}
