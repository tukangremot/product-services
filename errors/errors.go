package errors

import "errors"

var (
	UnauthorizedErr  = errors.New("Unauthorized")
	NoTokendErr      = errors.New("No Authorization Token provided")
	InvalidTokendErr = errors.New("The Authorization Token provided is invalid")
)
