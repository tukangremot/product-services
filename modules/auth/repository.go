package auth

import (
	"context"
	"database/sql"
)

type RepositoryInterface interface {
	GetUserByEmail(ctx context.Context, email string) (User, error)
	CreateUser(ctx context.Context, user User) error
}

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) RepositoryInterface {
	return &Repository{
		db: db,
	}
}

func (repository *Repository) GetUserByEmail(ctx context.Context, email string) (User, error) {
	var user User
	err := repository.db.QueryRow("SELECT id, email, name, password FROM users WHERE email=$1", email).Scan(&user.Id, &user.Email, &user.Name, &user.Password)
	switch err {
	case sql.ErrNoRows, nil:
		return user, nil
	default:
		return user, err
	}
}

func (repository *Repository) CreateUser(ctx context.Context, user User) error {
	sql := `
		INSERT INTO users (id, email, name, password)
		VALUES ($1, $2, $3, $4)`

	_, err := repository.db.ExecContext(ctx, sql, user.Id, user.Email, user.Name, user.Password)
	if err != nil {
		return err
	}
	return nil
}
