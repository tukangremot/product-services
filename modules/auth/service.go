package auth

import (
	"context"
	"errors"
	"net/mail"

	"github.com/gofrs/uuid"
	"golang.org/x/crypto/bcrypt"
	helperjwt "tukangremot.com/product-services/helpers/jwt"
)

var (
	EmailRequiredErr      = errors.New("Email is required")
	EmailDoesNotExistsErr = errors.New("Email does not exists")
	EmailFormatInvalidErr = errors.New("Invalid email format")
	EmailExistsErr        = errors.New("Email already exists")
	NameRequiredErr       = errors.New("Name is required")
	PasswordRequiredErr   = errors.New("Password is required")
	PasswordIvalidErr     = errors.New("Invalid Password")
)

type ServiceInterface interface {
	Login(ctx context.Context, email string, password string) (LoginResult, error)
	Register(ctx context.Context, name string, email string, password string) (string, error)
}

type Service struct {
	repostory RepositoryInterface
	jwtParams helperjwt.Params
}

func NewService(rep RepositoryInterface, jwtParams helperjwt.Params) ServiceInterface {
	return &Service{
		repostory: rep,
		jwtParams: jwtParams,
	}
}

type LoginResult struct {
	Id          string
	AccessToken string
}

func (service *Service) Login(ctx context.Context, email string, password string) (LoginResult, error) {
	var result LoginResult

	if email == "" {
		return result, EmailRequiredErr
	}

	if _, err := mail.ParseAddress(email); err != nil {
		return result, EmailFormatInvalidErr
	}

	if password == "" {
		return result, PasswordRequiredErr
	}

	// check email
	user, err := service.repostory.GetUserByEmail(ctx, email)
	if err != nil {
		return result, err
	}

	if (user == User{}) {
		return result, EmailDoesNotExistsErr
	}

	// check password
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return result, PasswordIvalidErr
	}

	accessToken, _ := helperjwt.GenerateToken(service.jwtParams, user.Id)

	return LoginResult{
		Id:          user.Id,
		AccessToken: accessToken,
	}, nil
}

func (service *Service) Register(ctx context.Context, name string, email string, password string) (string, error) {
	if email == "" {
		return "", EmailRequiredErr
	}

	if _, err := mail.ParseAddress(email); err != nil {
		return "", EmailFormatInvalidErr
	}

	if name == "" {
		return "", NameRequiredErr
	}

	if password == "" {
		return "", PasswordRequiredErr
	}

	user, _ := service.repostory.GetUserByEmail(ctx, email)

	if (user != User{}) {
		return "", EmailExistsErr
	}

	uuid, _ := uuid.NewV4()
	id := uuid.String()
	bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)

	user = User{
		Id:       id,
		Email:    email,
		Name:     name,
		Password: string(bytes),
	}

	if err := service.repostory.CreateUser(ctx, user); err != nil {
		return "", err
	}

	return id, nil
}
