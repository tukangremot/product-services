package auth

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	Login    endpoint.Endpoint
	Register endpoint.Endpoint
}

func NewEndpoint(service ServiceInterface) Endpoint {
	return Endpoint{
		Login:    Login(service),
		Register: Register(service),
	}
}

/*
 * Login endpoint
 */
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Id          string `json:"id"`
	AccessToken string `json:"access_token"`
}

func Login(service ServiceInterface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(LoginRequest)
		loginData, err := service.Login(ctx, req.Email, req.Password)

		if err != nil {
			return LoginResponse{}, err
		}

		return LoginResponse{
			Id:          loginData.Id,
			AccessToken: loginData.AccessToken,
		}, nil
	}
}

/*
 * Register endpoint
 */

type RegisterRequest struct {
	Email    string `json:"email"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type RegisterResponse struct {
	Id string `json:"id"`
}

func Register(service ServiceInterface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(RegisterRequest)
		id, err := service.Register(ctx, req.Name, req.Email, req.Password)

		if err != nil {
			return RegisterResponse{}, err
		}

		return RegisterResponse{id}, nil
	}
}
