package auth

type User struct {
	Id       string `json:"id,omitempty"`
	Email    string `json:"email"`
	Name     string `json:"name"`
	Password string `json:"password"`
}
