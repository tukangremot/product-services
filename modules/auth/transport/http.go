package transport

import (
	"context"
	"encoding/json"
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	helpershttp "tukangremot.com/product-services/helpers/http"
	"tukangremot.com/product-services/modules/auth"
)

func NewHTTPServer(ctx context.Context, router *mux.Router, endpoint auth.Endpoint) *mux.Router {

	router.Use(helpershttp.HeaderResponse)
	options := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(helpershttp.EncodeErrorResponse),
	}

	router.Methods("POST").Path("/v1/auth/login").Handler(kithttp.NewServer(
		endpoint.Login,
		DecodeLoginHttpRequest,
		helpershttp.EncodeSuccessResponse,
		options...,
	))

	router.Methods("POST").Path("/v1/auth/register").Handler(kithttp.NewServer(
		endpoint.Register,
		DecodeRegisterHttpRequest,
		helpershttp.EncodeCreatedResponse,
		options...,
	))

	return router

}

/*
 * Start decoder
 */

// login request decoder
func DecodeLoginHttpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request auth.LoginRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

// register request decoder
func DecodeRegisterHttpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request auth.RegisterRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

/*
 * end decoder
 */
