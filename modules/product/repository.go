package product

import (
	"context"
	"database/sql"
)

type RepositoryInterface interface {
	CreateProduct(ctx context.Context, product Product) error
	UpdateProduct(ctx context.Context, product Product) error
	GetProduct(ctx context.Context, id string) (Product, error)
	DeleteProduct(ctx context.Context, id string) error
	ListProduct(ctx context.Context) ([]Product, error)
}

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) RepositoryInterface {
	return &Repository{
		db: db,
	}
}

func (repository *Repository) CreateProduct(ctx context.Context, product Product) error {
	sql := `
		INSERT INTO products (id, name, type, weight, price, created_by)
		VALUES ($1, $2, $3, $4, $5, $6)`

	_, err := repository.db.ExecContext(ctx, sql, product.Id, product.Name, product.Type, product.Weight, product.Price, product.CreatedBy)
	if err != nil {
		return err
	}
	return nil
}

func (repository *Repository) UpdateProduct(ctx context.Context, product Product) error {
	sql := `
		UPDATE products SET
			name=$1,
			type=$2,
			weight=$3,
			price=$4,
			updated_by=$5
		WHERE id=$6`

	_, err := repository.db.ExecContext(ctx, sql, product.Name, product.Type, product.Weight, product.Price, product.UpdatedBy, product.Id)
	if err != nil {
		return err
	}
	return nil
}

func (repository *Repository) GetProduct(ctx context.Context, id string) (Product, error) {
	var product Product
	var UpdatedBy sql.NullString

	err := repository.db.QueryRow("SELECT id, name, type, weight, price, created_by, updated_by FROM products WHERE id=$1", id).Scan(&product.Id, &product.Name, &product.Type, &product.Weight, &product.Price, &product.CreatedBy, &UpdatedBy)
	switch err {
	case sql.ErrNoRows, nil:
		product.UpdatedBy = UpdatedBy.String
		return product, nil
	default:
		return product, err
	}
}

func (repository *Repository) DeleteProduct(ctx context.Context, id string) error {
	sql := `
		DELETE FROM products WHERE id=$1`

	_, err := repository.db.ExecContext(ctx, sql, id)
	if err != nil {
		return err
	}
	return nil
}

func (repository *Repository) ListProduct(ctx context.Context) ([]Product, error) {
	var result []Product

	rows, err := repository.db.Query("SELECT id, name, type, weight, price, created_by, updated_by FROM products")
	if err != nil {
		return result, err
	}

	for rows.Next() {
		var product Product
		var UpdatedBy sql.NullString
		err = rows.Scan(&product.Id, &product.Name, &product.Type, &product.Weight, &product.Price, &product.CreatedBy, &UpdatedBy)
		if err != nil {
			return result, err
		}

		product.UpdatedBy = UpdatedBy.String

		result = append(result, product)
	}

	return result, nil
}
