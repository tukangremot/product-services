package product

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	helpershttp "tukangremot.com/product-services/helpers/http"
)

type Endpoint struct {
	CreateProduct endpoint.Endpoint
	UpdateProduct endpoint.Endpoint
	DeleteProduct endpoint.Endpoint
	ViewProduct   endpoint.Endpoint
	ListProduct   endpoint.Endpoint
}

func NewEndpoint(service ServiceInterface) Endpoint {
	return Endpoint{
		CreateProduct: CreateProduct(service),
		UpdateProduct: UpdateProduct(service),
		DeleteProduct: DeleteProduct(service),
		ViewProduct:   ViewProduct(service),
		ListProduct:   ListProduct(service),
	}
}

/*
 * Create product endpoint
 */
type CreateProductRequest struct {
	Name      string `json:"name"`
	Type      string `json:"type"`
	Weight    int16  `json:"weight"`
	Price     int64  `json:"price"`
	CreatedBy string `json:"created_by"`
}

type CreateProductResponse struct {
	Id string `json:"id,omitempty"`
}

func CreateProduct(service ServiceInterface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(helpershttp.RequestData)
		data := req.Data.(CreateProductRequest)
		id, err := service.CreateProduct(ctx, data.Name, data.Type, data.Weight, data.Price, data.CreatedBy)

		if err != nil {
			return CreateProductResponse{}, err
		}

		return CreateProductResponse{id}, nil
	}
}

/*
 * Update product endpoint
 */
type UpdateProductRequest struct {
	Id        string `json:"id,omitempty"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Weight    int16  `json:"weight"`
	Price     int64  `json:"price"`
	UpdatedBy string `json:"updated_by"`
}

type UpdateProductResponse struct {
	Id string `json:"id,omitempty"`
}

func UpdateProduct(service ServiceInterface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(helpershttp.RequestData)
		data := req.Data.(UpdateProductRequest)
		id, err := service.UpdateProduct(ctx, data.Id, data.Name, data.Type, data.Weight, data.Price, data.UpdatedBy)

		if err != nil {
			return UpdateProductResponse{}, err
		}

		return UpdateProductResponse{id}, nil
	}
}

/*
 * Delete Product endpoint
 */
type DeleteProductRequest struct {
	Id string `json:"id"`
}

type DeleteProductResponse struct {
	Id string `json:"id,omitempty"`
}

func DeleteProduct(service ServiceInterface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(helpershttp.RequestData)
		id, err := service.DeleteProduct(ctx, req.Data.(DeleteProductRequest).Id)

		if err != nil {
			return DeleteProductResponse{}, err
		}

		return DeleteProductResponse{id}, nil
	}
}

/*
 * View Product endpoint
 */
type ViewProductRequest struct {
	Id string `json:"id"`
}

type ViewProductResponse struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Weight    int16  `json:"weight"`
	Price     int64  `json:"price"`
	CreatedBy string `json:"created_by"`
	UpdatedBy string `json:"updated_by"`
}

func ViewProduct(service ServiceInterface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(helpershttp.RequestData)
		product, err := service.ViewProduct(ctx, req.Data.(ViewProductRequest).Id)

		if err != nil {
			return ViewProductResponse{}, err
		}

		return ViewProductResponse{
			Id:        product.Id,
			Name:      product.Name,
			Type:      product.Type,
			Weight:    product.Weight,
			Price:     product.Price,
			CreatedBy: product.CreatedBy,
			UpdatedBy: product.UpdatedBy,
		}, nil
	}
}

/*
 * List Product endpoint
 */

func ListProduct(service ServiceInterface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		products, err := service.ListProduct(ctx)

		if err != nil {
			return products, err
		}

		return products, nil
	}
}
