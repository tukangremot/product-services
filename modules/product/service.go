package product

import (
	"context"

	"github.com/gofrs/uuid"
)

type ServiceInterface interface {
	CreateProduct(ctx context.Context, name string, productType string, weight int16, price int64, createdBy string) (string, error)
	UpdateProduct(ctx context.Context, id string, name string, productType string, weight int16, price int64, updatedBy string) (string, error)
	DeleteProduct(ctx context.Context, id string) (string, error)
	ViewProduct(ctx context.Context, id string) (Product, error)
	ListProduct(ctx context.Context) ([]Product, error)
}

type Service struct {
	repostory RepositoryInterface
}

func NewService(rep RepositoryInterface) ServiceInterface {
	return &Service{
		repostory: rep,
	}
}

func (service Service) CreateProduct(ctx context.Context, name string, productType string, weight int16, price int64, createdBy string) (string, error) {
	uuid, _ := uuid.NewV4()
	id := uuid.String()
	product := Product{
		Id:        id,
		Name:      name,
		Type:      productType,
		Weight:    weight,
		Price:     price,
		CreatedBy: createdBy,
	}

	if err := service.repostory.CreateProduct(ctx, product); err != nil {
		return "", err
	}

	return id, nil
}

func (service Service) UpdateProduct(ctx context.Context, id string, name string, productType string, weight int16, price int64, updatedBy string) (string, error) {
	product := Product{
		Id:        id,
		Name:      name,
		Type:      productType,
		Weight:    weight,
		Price:     price,
		UpdatedBy: updatedBy,
	}

	if err := service.repostory.UpdateProduct(ctx, product); err != nil {
		return "", err
	}

	return id, nil
}

func (service Service) DeleteProduct(ctx context.Context, id string) (string, error) {
	if err := service.repostory.DeleteProduct(ctx, id); err != nil {
		return "", err
	}

	return id, nil
}

func (service Service) ViewProduct(ctx context.Context, id string) (Product, error) {
	product, err := service.repostory.GetProduct(ctx, id)
	if err != nil {
		return Product{}, err
	}

	return product, nil
}

func (service Service) ListProduct(ctx context.Context) ([]Product, error) {
	products, err := service.repostory.ListProduct(ctx)
	if err != nil {
		return []Product{}, err
	}

	return products, nil
}
