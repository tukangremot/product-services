package transport

import (
	"context"
	"encoding/json"
	"net/http"

	kitendpoint "github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	helpershttp "tukangremot.com/product-services/helpers/http"
	helperjwt "tukangremot.com/product-services/helpers/jwt"
	middlewarehttp "tukangremot.com/product-services/middleware/http"
	"tukangremot.com/product-services/modules/product"
)

func NewHTTPServer(ctx context.Context, router *mux.Router, endpoint product.Endpoint, jwtParams helperjwt.Params) *mux.Router {

	router.Use(helpershttp.HeaderResponse)
	options := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(helpershttp.EncodeErrorResponse),
	}
	router.Methods("GET").Path("/v1/products").Handler(kithttp.NewServer(
		kitendpoint.Chain(middlewarehttp.Auth(jwtParams))(endpoint.ListProduct),
		DecodeListProductHttpRequest,
		helpershttp.EncodeSuccessResponse,
		options...,
	))

	router.Methods("POST").Path("/v1/products").Handler(kithttp.NewServer(
		kitendpoint.Chain(middlewarehttp.Auth(jwtParams))(endpoint.CreateProduct),
		DecodeCreateProductHttpRequest,
		helpershttp.EncodeCreatedResponse,
		options...,
	))

	router.Methods("PUT").Path("/v1/products/{id}").Handler(kithttp.NewServer(
		kitendpoint.Chain(middlewarehttp.Auth(jwtParams))(endpoint.UpdateProduct),
		DecodeUpdateProductHttpRequest,
		helpershttp.EncodeSuccessResponse,
		options...,
	))

	router.Methods("DELETE").Path("/v1/products/{id}").Handler(kithttp.NewServer(
		kitendpoint.Chain(middlewarehttp.Auth(jwtParams))(endpoint.DeleteProduct),
		DecodeDeleteProductHttpRequest,
		helpershttp.EncodeSuccessResponse,
		options...,
	))

	router.Methods("GET").Path("/v1/products/{id}").Handler(kithttp.NewServer(
		kitendpoint.Chain(middlewarehttp.Auth(jwtParams))(endpoint.ViewProduct),
		DecodeViewProductHttpRequest,
		helpershttp.EncodeSuccessResponse,
		options...,
	))

	return router

}

/*
 * Start decoder
 */

// create product request decoder
func DecodeCreateProductHttpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var data product.CreateProductRequest
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		return nil, err
	}
	var request = helpershttp.BuildRequestData(r, data)

	return request, nil
}

// update product request decoder
func DecodeUpdateProductHttpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var data product.UpdateProductRequest
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		return nil, err
	}

	vars := mux.Vars(r)
	data.Id = vars["id"]
	var request = helpershttp.BuildRequestData(r, data)

	return request, nil
}

// delete product request decoder
func DecodeDeleteProductHttpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)

	var data = product.DeleteProductRequest{
		Id: vars["id"],
	}
	var request = helpershttp.BuildRequestData(r, data)

	return request, nil
}

// view product request decoder
func DecodeViewProductHttpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	var data = product.ViewProductRequest{
		Id: vars["id"],
	}
	var request = helpershttp.BuildRequestData(r, data)

	return request, nil
}

func DecodeListProductHttpRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var data interface{}
	var request = helpershttp.BuildRequestData(r, data)

	return request, nil
}

/*
 * end decoder
 */
