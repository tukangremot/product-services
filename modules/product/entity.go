package product

type Product struct {
	Id        string `json:"id,omitempty"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Weight    int16  `json:"weight"`
	Price     int64  `json:"price"`
	CreatedBy string `json:"created_by,omitempty"`
	UpdatedBy string `json:"updated_by,omitempty"`
}
