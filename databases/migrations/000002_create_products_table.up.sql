CREATE TABLE "products" (
    "id" uuid PRIMARY KEY,
    "name" varchar NOT NULL,
    "type" varchar NOT NULL,
    "weight" int NOT NULL,
    "price" bigint NOT NULL,
    "created_by" uuid NULL,
    "updated_by" uuid NULL
);

CREATE INDEX ON "products" ("created_by");
CREATE INDEX ON "products" ("updated_by");