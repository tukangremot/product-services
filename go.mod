module tukangremot.com/product-services

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-kit/kit v0.11.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.2
	github.com/tukangremot/go-helpers v0.1.3
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
)
