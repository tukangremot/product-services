#!/bin/bash

mkdir -p `pwd`/docs/coverage
go test -coverprofile `pwd`/docs/coverage/coverprofile.out ./...
echo "\n== RESUME ==="
go tool cover -func `pwd`/docs/coverage/coverprofile.out
go tool cover -html=`pwd`/docs/coverage/coverprofile.out -o docs/coverage/index.html  