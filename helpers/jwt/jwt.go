package jwt

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	gojwt "github.com/dgrijalva/jwt-go"
)

type Params struct {
	SecretKey  []byte
	Expiration int
}

type Claims struct {
	Identifier string `json:"identifier"`
	gojwt.StandardClaims
}

func GenerateToken(params Params, identifier string) (string, error) {
	claims := Claims{
		identifier,
		gojwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Second * time.Duration(params.Expiration)).Unix(),
			IssuedAt:  gojwt.TimeFunc().Unix(),
		},
	}

	token := gojwt.NewWithClaims(gojwt.SigningMethodHS256, claims)
	return token.SignedString(params.SecretKey)
}

func ParsingToken(params Params, tokenString string) (*gojwt.Token, error) {
	return gojwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {

		if method, ok := token.Method.(*gojwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Signing method invalid")
		} else if method != gojwt.SigningMethodHS256 {
			return nil, fmt.Errorf("Signing method invalid")
		}

		return params.SecretKey, nil
	})
}
