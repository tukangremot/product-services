package http

import (
	"context"
	"encoding/json"
	"net/http"

	helpershttp "github.com/tukangremot/go-helpers/http"
	e "tukangremot.com/product-services/errors"
)

type RequestData struct {
	Token string `json:"token"`
	Data  interface{}
}

func BuildRequestData(r *http.Request, data interface{}) RequestData {
	var request = RequestData{
		Token: GetToken(r),
		Data:  data,
	}

	return request
}

// get token
func GetToken(r *http.Request) string {
	var token = ""

	if r.Header["Authorization"] != nil {
		token = r.Header["Authorization"][0]
	}

	return token
}

// set response header
func HeaderResponse(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json; charset=utf-8")
		next.ServeHTTP(w, r)
	})
}

// error response encoder
func EncodeErrorResponse(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	case e.UnauthorizedErr:
		w.WriteHeader(http.StatusUnauthorized)
	case e.NoTokendErr:
		w.WriteHeader(http.StatusUnauthorized)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	newResponse := helpershttp.BuildHttpResponseBody(false, err.Error(), nil)

	json.NewEncoder(w).Encode(newResponse)
}

// success response encoder
func EncodeSuccessResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	newResponse := helpershttp.BuildHttpResponseBody(true, "", response)
	return json.NewEncoder(w).Encode(newResponse)
}

// created response encoder
func EncodeCreatedResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.WriteHeader(http.StatusCreated)
	return EncodeSuccessResponse(ctx, w, response)
}
