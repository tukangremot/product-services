package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	helperjwt "tukangremot.com/product-services/helpers/jwt"
	"tukangremot.com/product-services/modules/auth"
	authtransport "tukangremot.com/product-services/modules/auth/transport"
	"tukangremot.com/product-services/modules/product"
	producttransport "tukangremot.com/product-services/modules/product/transport"
)

func main() {
	var (
		httpPort          = config("HTTP_PORT", "8080")
		httpAddr          = config("HTTP_ADDRESS", ":"+httpPort)
		dbSource          = config("POSTGRESQL_URL", "postgresql://postgres:postgres@localhost:5432/test_product?sslmode=disable")
		ctx               = context.Background()
		authSecretKey     = []byte(config("AUTH_SECRET_KEY", "rahasia"))
		authExpiration, _ = strconv.Atoi(config("AUTH_EXPIRATION", "120"))
	)

	// init logger
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"service", "products",
			"time:", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	// init postgres connection
	var db *sql.DB
	{
		var err error

		db, err = sql.Open("postgres", dbSource)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}

	}

	var authJwtParams = helperjwt.Params{
		SecretKey:  authSecretKey,
		Expiration: authExpiration,
	}

	// user handler
	var authEndpoint auth.Endpoint
	{
		authRepo := auth.NewRepository(db)
		authService := auth.NewService(authRepo, authJwtParams)
		authEndpoint = auth.NewEndpoint(authService)
	}

	// product handler
	var productEndpoint product.Endpoint
	{
		productRepo := product.NewRepository(db)
		productService := product.NewService(productRepo)
		productEndpoint = product.NewEndpoint(productService)
	}

	errs := make(chan error, 2)

	go func() {
		level.Info(logger).Log("transport", "http", "address", httpAddr, "msg", "service started")
		defer level.Info(logger).Log("transport", "http", "address", httpAddr, "msg", "msg", "service ended")
		var router *mux.Router
		{
			router = mux.NewRouter()
			router = authtransport.NewHTTPServer(ctx, router, authEndpoint)
			router = producttransport.NewHTTPServer(ctx, router, productEndpoint, authJwtParams)
		}
		errs <- http.ListenAndServe(httpAddr, router)
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	level.Error(logger).Log("terminated", <-errs)
}
