package http

import (
	"context"
	"strings"

	"github.com/go-kit/kit/endpoint"
	e "tukangremot.com/product-services/errors"
	helperhttp "tukangremot.com/product-services/helpers/http"
	helperjwt "tukangremot.com/product-services/helpers/jwt"
)

func Auth(params helperjwt.Params) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (interface{}, error) {
			req := request.(helperhttp.RequestData)

			if req.Token == "" {
				return nil, e.NoTokendErr
			}

			if !strings.Contains(req.Token, "Bearer") {
				return nil, e.InvalidTokendErr
			}

			tokenString := strings.Replace(req.Token, "Bearer ", "", -1)
			token, err := helperjwt.ParsingToken(params, tokenString)
			if err != nil {
				return nil, err
			}
			if !token.Valid {
				return nil, e.InvalidTokendErr
			}

			return next(ctx, request)
		}
	}
}
